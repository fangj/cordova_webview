//
//  Echo.h
//  HelloWorld
//
//  Created by Fang Jian on 14-11-28.
//
//

#import "CDVPlugin.h"

@interface Echo : CDVPlugin
- (void)echo:(CDVInvokedUrlCommand*)command;
@end
