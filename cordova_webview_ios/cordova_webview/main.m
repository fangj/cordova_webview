//
//  main.m
//  cordova_webview
//
//  Created by Fang Jian on 14-11-29.
//  Copyright (c) 2014年 Fang Jian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
