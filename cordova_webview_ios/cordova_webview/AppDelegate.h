//
//  AppDelegate.h
//  cordova_webview
//
//  Created by Fang Jian on 14-11-29.
//  Copyright (c) 2014年 Fang Jian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

