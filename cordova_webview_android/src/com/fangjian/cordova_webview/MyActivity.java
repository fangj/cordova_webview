package com.fangjian.cordova_webview;

import android.os.Build;
import android.os.Bundle;
import org.apache.cordova.CordovaActivity;

public class MyActivity  extends CordovaActivity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            this.appView.setWebContentsDebuggingEnabled(true);
        }
        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);

    }
}
